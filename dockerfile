FROM ubuntu:latest
LABEL Spongebob Dockerpants "s.dockerpants@comcast.net"
RUN apt-get update && apt -y install python3 python3-pip curl

#Add source files
WORKDIR /app
COPY . /app

# Install Python web server and dependencies
RUN pip3 install -r requirements.txt

# Expose port
EXPOSE 8090

#ENTRYPOINT ["python3"]

CMD python3 app.py

#CMD tail -f /dev/null
